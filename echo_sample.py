
from utils import set_axis_stuff
import wave
import struct
import numpy as np
import pandas as pd
from scipy.signal import chirp 
import os
import wave
import time
from scipy.io import wavfile
from scipy import signal
from scipy import constants
import numpy as np
import matplotlib
import math
# try: 
#     matplotlib.use("module://mplcairo.gtk")
# except:
#     pass

from scipy import fftpack
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter
import matplotlib.ticker as mticker
import threading
#import jack
import socket
import queue
import collections
#from numba import jit
from calc import Schnittpunkt, MicPos, chirpgenerator,butter_bandpass_filter,convert
from echognition import STFT_RIR
from numpy import asarray as ar,exp
from mpl_toolkits.mplot3d import axes3d
import openpyscad as ops
#from numba import jit

def RIR_calc(datastorage,channels,fstart=1000,fstop=19000,loadpath="",fs=192000,):#noch kein Channel Stuff # Ausrechnen vom RIR und peakfinding    
    n=0
    firstchirp= 8
    lastchirp = 8
    channels=2
    # get number of recorded chirps in folder
    for root, dirs, recs in os.walk(loadpath):
        for name in recs:
            if name=="chirp.npy":
                chirp_values=np.load(os.path.join(root,name)) 
            elif name[-1]=="d":
                e=0
            elif name[-1]=="e":
                e=0
            elif firstchirp <=int(name[0:-4]) <=lastchirp:
                datastorage.put(np.load(os.path.join(root,name)))
    chirplen=len(chirp_values)
    cors=np.zeros((channels,chirplen))

    while True:
        #load previous recording, or get from queue
        try: 
            data = datastorage.get(timeout=6)
            n+=1
        except queue.Empty:  #wenn nix is mach nix
            break


        for c in range(channels):
            rec_chirp = data[c] #select channel
            #rec_chirp = butter_bandpass_filter(rec_chirp, fstart, fstop, fs, order=6)#Filter
            corr_chirp = np.correlate(rec_chirp,chirp_values,"same")#correlate
            corr_chirp = corr_chirp/np.max(corr_chirp)#normalize
            peaks,peaks_dict=signal.find_peaks(corr_chirp,height=(0.07,None))
            chirp_loc=0 # location of original chirp 
            for index in range(chirplen):
                if corr_chirp[index]>corr_chirp[chirp_loc]:
                    chirp_loc=index
            corr = np.append(corr_chirp[chirp_loc:],np.zeros(chirp_loc))
            plt.subplot(channels*100+10+c+1)
            plt.scatter([peaks[0]/192000,chirp_loc/192000], corr_chirp[[peaks[0],chirp_loc]], marker="x", s=30, label="Peaks",color="orange")
            plt.hlines(corr_chirp[peaks[0]],peaks[0]/192000,chirp_loc/192000,colors="red")
            plt.plot(np.linspace(0,chirplen/192000,chirplen),corr_chirp,label="mic: "+str(c),color="blue")
            plt.xlabel("Zeit in s")
            plt.ylabel("Amplitude")
            plt.legend(loc="upper right")
            #if chirplen-chirp_loc>echo_lens[c]: echo_lens[c]=chirplen-chirp_loc
            cors[c]+=corr_chirp
            print("Laufweg Unterschied",(chirp_loc/192000-peaks[0]/192000)*constants.mach,) 
        print("Chirp done",n)
    for c in range(channels): 
        cors[c]=cors[c]/np.max(cors[c])  
    return cors
    


def peaksorter(cors,channels,fs):
    '''
    cors: correlations
    channels: number of channels
    chirplen:
    echo_lens: länge des echosignals in corr
    '''
    chirplen=len(cors[0])
    c_peaks=[1,1,1,1]
    for c in range(channels):
        print(c)
        cors[c]=cors[c]/np.max(cors[c]) #normalization
        
        p=0
        #start_y = 0.1
        change  = 0.2
        minichange=0.0001
        #pktgerade = np.linspace(0,(change/75)*chirplen,chirplen) #set up a line for prominence
        d=0.04
        #make e function
        strech = 2
        xs=np.linspace(0,50,chirplen)
        #plt.plot(np.linspace(0,chirplen/192000,chirplen),ys,color="green",label="prominence")
        #plt.show()
        last=int(chirplen/2)
        prominence=0.05
        peak_bol=True
        #Alternate=True
        while peak_bol:
            #apply prominence
            
            prominence=5**(-xs*strech)+d
            peaks_fil,_=signal.find_peaks(cors[c][0:chirplen],prominence=prominence,width=2)
            #peaks_fil=np.array([peaks[0]],dtype=int)
            #for i in range(1,len(peaks)-1):
             #   if True: #cors[c][peaks[i-1]]<cors[c][peaks[i]]>cors[c][peaks[i+1]] or (cors[c][peaks[i+1]]-cors[c][peaks[i]] > 500 and cors[c][peaks[i]]- cors[c][peaks[i-1]] >500):
              #      peaks_fil=np.append(peaks_fil,int(peaks[i])) 
                           
            #stop when more than ten and less than 25 are detected
            peak_bol=False
            
            #if not Alternate:
            #peak_bol=True
            if peaks_fil.size > 0:
                if peaks_fil[0] > 1800:
                    strech+=change
                    #print("no floor")
                    Alternate=True
                    peak_bol=True

            #elif Alternate:
            if peaks_fil.size>20:
                d+=minichange
                #print("too many")
                peak_bol=True
                Alternate=False
            if peaks_fil.size<12:
                d-=minichange
                #print("too few")
                peak_bol=True
                Alternate=False
            

        #plt.subplot(channels*100+10+c+1)  
        #plt.plot(np.linspace(0,chirplen/192000,chirplen),prominence,color="green")
        #plt.plot(np.linspace(0,chirplen/192000,chirplen),prominence,color="green",label="prominence")
        #plt.scatter(peaks_fil/192000, cors[c][peaks_fil], marker="x", s=30, label="Peaks",color="orange")
        plt.plot(np.linspace(0,chirplen/192000,chirplen),cors[c],label="mic: "+str(c),color="blue")
        # erwartung für M1
        #plt.vlines([0.00325736, 0.00139042, 0.01786965, 0.00836897, 0.01740664, 0.03002049],-1,1,colors=["Red","pink","lightblue","green","yellow","orange"])
        # erwartung M0
        #plt.vlines([0.00223788, 0.00212202, 0.01881281, 0.00860611, 0.01778782, 0.02966544],-1,1,colors=["Red","pink","lightblue","green","yellow","orange"])
        plt.xlabel("Zeit in s")
        plt.ylabel("Amplitude")
        plt.legend(loc="upper right") 
        c_peaks[c]=peaks_fil
    #print(c_peaks)  
    return c_peaks #return sorted peaks

#@jit(nopython=True)
def Roomcalc(Pos,Dis,echoes): 
    ImgSrcs=[]
    place_peaks = pd.DataFrame(columns=("x","y","z","p1","p2","p3","p4","error"))
    errors=[]
    n=0 
    tolerance=0.5
    
    # Dis[i][j] : Distanz der Mikrofone
    # p_i : peak/echo von mic i
    
    # Iterate all echos from mic 1
    for p1 in echoes[0]: # Go through all combinations of peaks and ignore combos without intersection
        # Iterate all echos from mic 2
        for p2 in echoes[1]:
            if p1+p2<Dis[1][2] or abs(p1-p2) > Dis[1][2]+tolerance:
                continue
            # Iterate all echos from mic 3
            for p3 in echoes[2]:
                if p1+p3<Dis[1][3] or p2+p3<Dis[2][3] or abs(p1-p3) >Dis[3][1] +tolerance or abs(p2-p3) >Dis[3][2] +tolerance:
                    continue
                # Iterate all echos from mic 4
                for p4 in echoes[3]:
                    if p1+p4<Dis[1][4] or p2+p4<Dis[2][4] or p3+p4<Dis[3][4] or abs(p1-p4) > Dis[1][4]+tolerance or abs(p2-p4) > Dis[4][2]+tolerance or abs(p3-p4) > Dis[3][4] + tolerance:
                        continue
                    try:
                        Point, error = Schnittpunkt(Pos,p1,p2,p3,p4)
                        if Point is None:
                            continue
                    except AttributeError as e:
                        print(e.with_traceback())
                        continue
                    
                    
                    Data_A = pd.DataFrame({
                                            "x":Point[0]/2,
                                            "y": Point[1]/2,
                                            "z":Point[2]/2,
                                            "p1":p1,
                                            "p2":p2,
                                            "p3":p3,
                                            "p4":p4,
                                            "error":error,
                                        },index=[n]) 
                    place_peaks=pd.concat([place_peaks,Data_A])  
                    ImgSrcs.append(Point)
                    errors.append(error)

                    n+=1 
        
    return np.array(ImgSrcs), np.array(errors),place_peaks


def main():
    #mode  
    plorting=True
    channels = 1
    fs = 192000
    #Parameters
    #EntfernungsMatrix Sporthalle
    #    L   M0   M1   M2   M3
    # Dis=[[0,3.35,4.12,1.72,3.85],#L
    #      [3.35,0,1.78,1.86,2.15],#Mic0
    #      [4.12,1.78,0,1.42,0.75],#Mic1
    #      [1.72,1.86,1.42,0,2.23],#Mic2
    #      [3.85,2.15,0.75,2.23,0]]#Mic3
    
    #Gemeindehaus
    #    L   M0   M1   M2   M3
    # Dis=[[0,2.16,2.45,1.17,2.27],#L
    #       [2.16,0,1.71,1.26,1.72],#Mic0
    #       [2.45,1.71,0,1.63,1.30],#Mic1
    #       [1.17,1.26,1.63,0,1.22],#Mic2
    #       [2.27,1.72,1.30,1.22,0]]#Mic3

    #MultiRaum
    # Dis = [[0,4.85,2.85,4.92,4.40],
    #        [4.85,0,3.60,2.73,5.76],
    #        [2.85,3.60,0,2.56,2.78],
    #        [4.92,2.73,2.56,0,2.58],
    #        [4.40,5.76,2.78,2.58,0]]
    
    #MultiRaum 2
    Dis = [[0,3.71,3.68,4.25,4.21],
           [3.71,0,0.585,0.64,0.815],
           [3.68,0.585,0,0.66,0.90],
           [4.25,0.64,0.66,0,0.77],
           [4.21,0.815,0.90,0.77,0]]

    #Multiraum 3 only D4
    # Dis = [[0,3.17,3.36,3.76,3.32],
    # [3.17,0,0.585,0.64,0.815],
    # [3.36,0.585,0,0.66,0.90],
    # [3.76,0.64,0.66,0,0.77],
    # [3.32,0.815,0.90,0.77,0]]

    #read parameters of client
    

    

    datastorage = queue.Queue()# for chirp analysis
    
    fstart=1000
    fstop=19000
  
    loadpath="./app/Recordings/W_b_sun_15-19/"


    cors= RIR_calc(datastorage,channels,fstart, fstop, loadpath,fs)
    np.save("./app/_tempcorr.npy",cors)
    #cors = np.load("./app/_tempcorr.npy")

    
    peaks = peaksorter(cors,channels,fs)
    #plt.savefig("corr_Whiten_M.svg")
    plt.show()
    #np.save("_temppeaks",peaks)
    return
    #peaks   = np.load("_temppeaks.npy",allow_pickle=True)
    echoes = np.load("_tempechoes.npy",allow_pickle=True)
    #echoes  = convert(Dis,peaks,fs,channels)
    Pos,err     = MicPos(Dis)
    Imgsrcs,error,place_peaks = Roomcalc(Pos,Dis,echoes)
    place_peaks.to_pickle("place_peak_perfect_D.pkl")
    #place_peaks=pd.read_pickle("place_peak_D2_new.pkl")
    #pd.set_option("max_rows", 11)
    #print(place_peaks.describe())
    #print(Pos)
    scad=ops.Union()
    speakerIdx = 0

    SIZEMULTIPLIER = 7 #tbs
    for i in range(len(place_peaks['x'])):
        dist = abs(math.sqrt(place_peaks['x'][i]**2 + place_peaks['y'][i]**2 + place_peaks['z'][i]**2))
        scad.append(ops.Sphere(r=0.15, _fn=10).translate([place_peaks['x'][i], place_peaks['y'][i], place_peaks['z'][i]]).color("Red"))#,(place_peaks["error"][i])*2/max(error)))
    '''
    for i in range(len(Imgsrcs[0])):
        dist = abs(math.sqrt(Imgsrcs[0][i]**2 + Imgsrcs[1][i]**2 + Imgsrcs[2][i]**2))
        scad.append(ops.Sphere(r=SIZEMULTIPLIER*dist, _fn=10).translate([Imgsrcs[0][i], Imgsrcs[1][i],Imgsrcs[2][i]]).color("Red"))
    '''
    # Iterate speaker and microphones
    for i in range(channels+1):
        if i == speakerIdx: # Catch that speaker and paint it orange
            scad.append(ops.Sphere(r=0.05, _fn=10).translate([Pos[i][0], Pos[i][1],Pos[i][2]]).color("Orange"))
        else: # Paint the mics in blue
            scad.append(ops.Sphere(r=0.05, _fn=10).translate([Pos[i][0], Pos[i][1],Pos[i][2]]).color("Blue"))

    scad.write("D_perfect_t=0.05.scad")      
    return
    print("Alle Datenpunkte")
    print(place_peaks.to_string())


    if plorting:
        plt.show()
    print('Good Exit!')


if __name__ == "__main__":
    # import sys
    # print(str(sys.argv))
    main()
