# Aufnahme D Multiraum
## 13898288

# Distanzmatrix
D = [[0,3.71,3.68,4.25,4.21],
    [3.71,0,0.585,0.64,0.815],
    [3.68,0.585,0,0.66,0.90],
    [4.25,0.64,0.66,0,0.77],
    [4.21,0.815,0.90,0.77,0]]

1-19kHz
0.5 s

## Chirps
1-14


- Trigger-
- Im Multiraum 
- Stühle in der Ecke zwischen a und d
- Fenster bei b
- diagonale d
- Vorsprung bei a, bzw Treppe
- rechter Winkel bei ab und cb


## Lautsprecher
Lautsprecher auf Eingang gerichtet 

# Raummaße 
Trapezform 
- a = 8.52m
- c = 7.38m
- b = 10.50m
- h = 3.00m
- d = sqrt((a-c)**2 + b**2)  = 10.56
Volumen = 

OpenSCAD Code für Multiraum:
Points= [
[-2.53,6.3,-1.3],
[7.97,6.3,-1.3],
[7.97,-1.08,-1.3],
[-2.53,-2.22,-1.3],
[-2.53,6.3,1.7],
[7.97,6.3,1.7],
[7.97,-1.08,1.7],
[-2.53,-2.22,1.7]];

Faces= [
[0,1,2,3],//bot
[4,5,6,7],//top
[0,3,7,4],//a
[0,1,5,4],//b
[1,2,6,5],//c
[2,3,7,6]];//d
color(c= [0,255,0], alpha = 0.5 ){polyhedron(Points,Faces);};


# Koordinaten und vektoren
Abstand L zu b = 6.30
L zu d = 1.85
L zu a = 2.53
L zu c = 7.65
L = (0,0,0)
M3 zu b = 3.47
M3 zu c= 4.63
Eckpunkt(ab)unten = ausrechnen mit Lautsprecher koords
Abstand von den Mikrofonen zum Boden (meter) 
L = 1.30
M0 = 1.765- 1.30 = 0.465
M1 = 1.23 - 1.30 =- 0.07
M2 = 1.63 -1.30 = 0.33
M3 = 1.405 -1.30 = 0.105
