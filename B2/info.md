# Sporthalle 

## Aufnahme 2

Useful Chirps: 3-10

### JACK Einstellung
samplerate: 192000
blocksize:4096
Period/Buffer: 3
Latenz: 64 ms (von JACK)
### Chirp
Frequenz: 1kHz-19kHz
Dauer: 1 s
Typ: hyp
### Beschreibung:
- 4 mikrofone im fast leeren Raum 
- gepolsterte Wände etwa 2.5m hoch
- Abstandsmatrix bekannt
- chirp war sehr leicht verzerrt sollte kein Problem sein
- Decke unsicher, angewinkeltes Schenkeldach

### Raumdimensionen: 
Länge = 45 m Ost-Wests(y)
Breite = 28 m Nord-Süd( x)
Höhe = 9+-1 m Oben-Unten


### Abstandsmatrix:
    L    M0   M1     M2   M3
L   x   3.35  4.12  1.72  3.85
M0  x    x    1.78  1.86  2.15  
M1  x    x    x     1.42  0.75
M2  x    x    x      x    2.23
M3  x    x    x      x    x
