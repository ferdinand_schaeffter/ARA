# ARA - Akustische Raum Analyse

Dieses Projekt dreht sich um die Erkennung von Räumen mithilfe von Lautsprecher und mindestens 4 Mikrofonen. Das Aufnahmeprogramm ARA-JACK.py funktioniert nur mit Ubuntu 18.0.4 und dem Audioprogramm JACK. Es sind Beispielaufnahmen in den Ordnern A1 (Gemeindehaus) und B2 (Sporthalle) zu finden, in denen zudem eine Infodatei und das Ausgangssignal als numpy datei gespeichert sind.

1. Aufnahme
Die Aufnahme erfolgt mit ARA-JACK.py. Hierbei muss das Ausgangssignal definiert werden (Frequenzbereich und Dauer). Außerdem müssen 4 Mikrofone angeschlossen sein. Für die Auswertung ist eine Distanzmatrix der Mikrofone und der Lautsprecher erforderlich, daher ist es sinnvoll vor der Aufnahme diese zu messen und in den einzutragen. Sobald Sie zufrieden sind mit der Anzahl an Aufnahmen, drücken Sie ENTER und beenden die Aufnahmeschleife.

2. Auswertung
Die Auswertung erfolgt mit echo_sample.py. Hierbei muss unter loadpath der Aufnahmeordner angegeben werden und die gewünschten Aufnahmen für die Mittelung müssen als Bereich unter First_chirp und last_chirp angegeben werden.

calc.py beinhaltet wichtige Funktionen für die Auswertung.
