# Aufbau 1 

## Aufnahme 1

Useful Chirps: 4-25

### JACK Einstellung
samplerate: 192000
blocksize:4096
Period/Buffer: 3
Latenz: 64 ms (von JACK)


### Beschreibung:
- 4 mikrofone im fast leeren Raum 
- Bühne Richtung Norden Vorhang und Vorsprung
- Fenster Richtung Westen
- Abstandsmatrix bekannt
- Koordinaten der Mikros und LAutsprecher bekannt
- chirp war leicht verzeert sollte kein Problem sein
- Lautsprecher steht im Winkel von etwa 15 Grad mathematisch positiv von der langen Wand aus
- Lukas u. Ferdi still in der Ecke

### Raumdimensionen: 
Länge = 7,14 m Ost-Wests(y)
Breite = 9,52 m Nord-Süd( x)
Höhe = 3,56 m Oben-Unten

### Koordinatensystem Ursprung = untere Ecke Nord-Ost 
(1e =1m)(x(Lange Seite-O)|y(kurze Seite-N)|Z(Höhe)) (0|0|0):
Lautsprecher: (1,15|2,68|0,3)
Mic0: (2,75|2,77|1,70)
Mic1: (3,18|4,02|0,66)
Mic2: (1,81|3,27|1,01)
Mic3: (2,15|4,30|1,38)

### Abstandsmatrix:
    L    M0   M1     M2   M3
L   x   2,16  2,45  1,17  2,27
M0  x    x    1,71  1,26  1,72  
M1  x    x    x     1,63  1,30
M2  x    x    x      x    1,22
M3  x    x    x      x    x
    #    L   M0   M1   M2   M3
    Dis=[[0,2.16,2.45,1.17,2.27],#L
         [2.16,0,1.71,1.26,1.72],#Mic0
         [2.45,1.71,0,1.63,1.30],#Mic1
         [1.17,1.26,1.63,0,1.22],#Mic2
         [2.27,1.72,1.30,1.22,0]]#Mic3