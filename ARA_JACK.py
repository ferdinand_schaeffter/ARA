# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'

# %%
from utils import set_axis_stuff
import wave
import struct
import numpy as np
from scipy.signal import chirp 
import os
import wave
import time
from scipy.io import wavfile
from scipy import signal
from scipy import constants
import numpy as np
import matplotlib

from scipy import fftpack
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter
import matplotlib.ticker as mticker
import threading
import logging
import jack
import socket
import queue
import collections
from numba import jit





# %%

#figure naming
def getNextFilePath(output_folder,name):
    highest_num = 0
    for f in os.listdir(output_folder):
        if os.path.isfile(os.path.join(output_folder, f)):
            file_name = os.path.splitext(f)[0]
            file_name=file_name[-1:]
            try:
                file_num = int(file_name)
                if file_num > highest_num:
                    highest_num = file_num
            except ValueError:
                'The file name "%s" is not an integer. Skipping' % file_name

    output_file = os.path.join(output_folder, name+str(highest_num + 1))
    return output_file
# %%
# Funktion die den Chirp abspielt wird in einem Thread gestartet
def playrec(client, play, recording, chirplen, chunked):
    t = threading.currentThread()
    def process(frames):
        data =play.get_nowait()
        for port in client.outports:
            port.get_array()[:] = data
        for port in client.inports:
            recording.put_nowait(port.get_array())
    
    #start client
    n=0
    client.set_process_callback(process)
    client.activate()
    play_port =client.outports.register("output")
    rec=[]
    channels=0
    
    # connect and register ports
    for port in client.get_ports(is_audio=True,is_physical=True,is_output=True):
        rec.append(client.inports.register("in"+str(channels)))
        client.connect(port,rec[channels])
        channels+=1

    for port in client.get_ports(is_audio=True,is_physical=True,is_input=True):
        play_port.connect(port)

    #for looping playback
    while getattr(t, "terminate", False):
        n+=1
        for i in range(chunked.shape[0]):
            play.put(chunked[i],timeout=10)
        print("gespielt:",n)
    
    client.deactivate()
    client.close()
        

def storage(datastorage, chirplen, recording ,channels,savepath, samplerate=44000,fstop=15000, blocksize=1024):
    t = threading.currentThread()
    maxamp=0.1 # muss ot angepasst werden
    triggerdelay = 0 # in Milliseconds
    delayblocks = int((triggerdelay * samplerate / (1000 * blocksize)))
    n=0
    chirpbuffer = [[]]*channels
    for c in range(channels):
        chirpbuffer[c]=collections.deque(maxlen = chirplen)
    def setmax(cur_max,cur_value):
        if cur_value > cur_max:
            print("new max:", cur_value)
            return cur_value
        else:
            return cur_max
    def fillbuffer():
        nonlocal chirpbuffer
        for c in range(channels):
            data_in = recording.get(timeout=10)
            for i in range(len(data_in)):
                chirpbuffer[c].append(data_in[i])
        return data_in

    while getattr(t, "terminate", False): #Running all the time
        current_in=fillbuffer() # könnte Probleme machen wenn buffer/blocksize zu klein         
        cf,ct,current_stft = signal.stft(current_in,fs=samplerate)
        index_fstop=0

        for i in range(len(cf)):
            if np.abs(cf[i]-fstop) < np.abs(cf[index_fstop]-fstop):
                index_fstop=i
        fstop_ampltiude = np.sum(np.abs(current_stft[index_fstop]))
        maxamp=setmax(maxamp,fstop_ampltiude)
        trigger = fstop_ampltiude>maxamp*0.5
        if trigger:
            for i in range(delayblocks): #delay für zu frühes auslösen
                if getattr(t, "terminate", False):
                    fillbuffer()
                else:
                    break
            chirpdata = np.zeros((channels, chirplen))
            if len(chirpbuffer[0]) != chirplen:# ist der Buffer voll?
                print("no full chirp")
                continue
            else:
                for c in range(channels):
                    for j in range(chirplen):
                        chirpdata[c][j] = chirpbuffer[c].popleft()    

            #recording over
            datastorage.put(chirpdata) # Send data off to calculation
            n+=1
            print("recorded chirp ",n)
            if getattr(t, "save", True):
                np.save(savepath+"/"+str(n),chirpdata)


def RIR_calc(datastorage,chirp_values,channels,loadpath="",fs=96000):#noch kein Channel Stuff # Ausrechnen vom RIR und peakfinding    
    return
    t = threading.currentThread()
    n=0
    nperseg = 44
    # get number of recorded chirps in folder
    if getattr(t, "load", True):  
        for root, dirs, recs in os.walk(loadpath):
            for name in recs:
                if name=="chirp.npy":
                    print("chirp loaded")
                    chirp_values=np.load(os.path.join(root,name)) 
                elif name[-1]=="d":
                    print("comment")
                else:
                    datastorage.put(np.load(os.path.join(root,name)))
    while getattr(t, "terminate", False):
        #load previous recording, or get from queue
        try: 
            data = datastorage.get(timeout=10)
            n+=1
        except queue.Empty:  #wenn nix is mach nix
            if getattr(t, "load", True):
                break
            else:
                continue
        corr_peaks=[[]]*channels

        print(f"Using {channels} mics")
        data=np.load(loadpath+"18.npy")
        for c in range(channels):
            rec_chirp = data[c] #select channel
            # 3. Mal mit mehreren Chirps testen
            corr_chirp = np.correlate(chirp_values,rec_chirp, "full")
            corr_chirp = corr_chirp/np.max(corr_chirp)
            peaks,_=signal.find_peaks(corr_chirp,height=(0.1,None),prominence=(0.07,None))
            corr_peaks[c]=peaks
            #print("max at: ",np.where(corr_chirp==np.max(corr_chirp))[0][0])
            corr_max=np.where(corr_chirp==np.max(corr_chirp))[0][0]
            T_offset = (corr_max-len(chirp_values))/fs
            print("Laufzeitdifferenz = ",T_offset, "s")
            print("Laufwegdifferenz  = ",T_offset*constants.mach,"m")

            #plorting
            # plt.subplot(211)
            # plt.scatter(range(len(corr_chirp)), corr_chirp,s=1,label="Full corr")
            # plt.scatter(corr_peaks[c], corr_chirp[corr_peaks[c]], marker="x", s=30, label="Peaks")
            # plt.vlines(x=[len(chirp_values)],ymin=-1,ymax=1,color="green")
            # plt.vlines(x=[corr_max],ymin=-1,ymax=1, color="red")
            # plt.xlabel(f"time in 1/{fs} seconds")
            # plt.ylabel("Amplitude normalized")
            # plt.legend()
            # plt.subplot(212)
            # plt.scatter(range(len(rec_chirp)),rec_chirp, s=1,label="recording")
            # plt.scatter(range(len(chirp_values)),1500*chirp_values,s=1,alpha=0.5, label="chirp")
            # plt.legend()
            

        print("RIR done",n)
        if not getattr(t,"load"):
            datastorage.task_done()
        #for i in range(channels):
        #    plt.scatter(tr[chirp_peaks[i]], RIR[chirp_peaks[i]], marker="x", s=1)
        break 
    print("out of loop")


def chirpgenerator(play,blocksize,fstart = 1000, fstop=15000,chirp_dur=0.3,fs = 96000,channels=1):    
    #samples?
    samples=int(fs*chirp_dur)
    overhead=samples % blocksize 
    samples = samples-overhead
    #t u. buffer
    t=np.linspace(0,chirp_dur,samples)
    buffer=np.zeros(2*samples)

    #chirp
    chirp_values=chirp(t,fstart,chirp_dur,fstop,method="hyp") # 10000 only for laptop recordings bc bad microphones
    values=np.append(chirp_values,buffer)
    values=np.append(buffer,values)
    # schreiben in die queue
    chunked=values.reshape(int(len(values)/blocksize),blocksize)
    #for i in range(chunked.shape[0]):
    #    play.put_nowait(chunked[i])
    return (chirp_values, samples, chunked)
    



def main():
    #mode
    Save=True # save generated chirp and recordings
    Load=False # in loading and analysis
    #setup client
    client = jack.Client("ARA")
    #read parameters of client
    block_size=client.blocksize
    channels = len(client.get_ports(is_audio=True,is_physical=True,is_output=True))
    fs = client.samplerate
    qsize=120 #Size of queueus used in process
    # initialize queue
    play = queue.Queue(maxsize=qsize)  
    recording = queue.Queue(maxsize=qsize)# for audio outputs
    datastorage = queue.Queue()# for chirp analysis
    # Generate Chirp on the fly
    fstop=5000
    chirp_values, chirplen, chunked = chirpgenerator(play,block_size,channels=channels,fs=fs,fstart=500,fstop=fstop,chirp_dur=0.8)
    print("chirplen: ", chirplen)
    print("channels: ", channels)
    print("blocksize: ", block_size)
    # Set Recording Path
    savepath = f"./app/Recordings/{int(time.time()-1602612611.730985)}"
    loadpath="./app/Recordings/5440275/"
    # save chirp 
    if Save == True:
        os.mkdir(savepath)
        np.save(savepath+"/chirp",chirp_values)

    

    # initialize Threads
    playThread = threading.Thread(target=playrec, args=(client, play, recording, chirplen, chunked)) # might cause problems if channels too  high
    storeThread = threading.Thread(target=storage, args=(datastorage, chirplen, recording,channels,savepath,fs,fstop))
    RIR_calcThread = threading.Thread(target=RIR_calc, args=(datastorage,chirp_values,channels,loadpath,fs))

    #set up
    playThread.terminate = not Load
    storeThread.terminate = not Load
    RIR_calcThread.terminate = True 
    RIR_calcThread.load = Load #True to load previously recorded data
    storeThread.save = Save #True to save data in .npy file

    # start Threads
    if not Load:
        playThread.start() 
        storeThread.start()
    RIR_calcThread.start()
    
    #wait for input the stop
    print('Press any key to stop')
    input()
    #Terminate Threads
    playThread.terminate = False
    storeThread.terminate = False
    RIR_calcThread.terminate = False
    if not Load:
        playThread.join()
        storeThread.join()
    RIR_calcThread.join()
    
    #plt.show()
    print('Good Exit!')
    
    
if __name__ == "__main__":
    time.sleep(1)
    main()

# %%
